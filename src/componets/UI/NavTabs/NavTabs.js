import React, {useEffect, useState} from 'react';
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import {Link, useLocation} from "react-router-dom";
import {ADD_NEW_CONTACT_ROUTE_LABEL} from "../../../constants";

const NavTabs = ({items, routes, currentContact}) => {
  const location = useLocation();

  const [value, setValue] = useState(() => {
    for (let i=0; i < routes.length; i++){
      if (location.pathname === routes[i]) return i;
    }
    return 0;
  });
   const onTabChange = (e, value) => setValue(value);

   useEffect(() => {
     setValue(() => {
       for (let i=0; i < routes.length; i++){
         if (location.pathname === routes[i]) return i;
       }
       return 0;
     });
   }, [routes, location])

   return (
     <Tabs value={value}
           onChange={onTabChange}
           textColor="secondary"
           indicatorColor="secondary"
     >
        {items.map((item) => {
          let changeableLabel = item.label;
          if (currentContact && item.label === ADD_NEW_CONTACT_ROUTE_LABEL) changeableLabel = 'Edit Contact';
           return (
                <Tab
                  key={item.path}
                  label={changeableLabel}
                  to={item.path}
                  component={Link}
                />
           );
        })}
     </Tabs>
   );
};

export default NavTabs;