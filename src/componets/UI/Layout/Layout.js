import React from 'react';
import {Box, Grid} from "@mui/material";
import MainNav from "../../../containers/MainNav/MainNav";

const Layout = ({children}) => {
  return (
    <div>
      <Grid container direction={"column"} sx={{borderBottom: '1px solid gainsboro'}} pb={2}>
        <Grid item alignSelf={"flex-end"}>
          <MainNav/>
        </Grid>
      </Grid>
      <Box p={2}>
        {children}
      </Box>
    </div>
  );
};

export default Layout;