import React from 'react';
import {Grid} from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import {styled} from "@mui/material/styles";

const Item = styled("div")(({theme}) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.primary,
}));

const style = {
  border: '1px solid gainsboro',
  cursor: 'pointer'
};

const ContactItem = (props) => {

  return (
    <Grid
      container
      justifyContent={"space-between"}
      alignItems={"center"}
      mb={2}
      onClick={props.onClickHandler}
      sx={style}
      maxWidth={'350px'}
      mx={'auto'}
    >
      <Grid item mr={1}>
        <CardMedia
          component="img"
          height="64"
          image={props.imgUrl}
          alt={props.name}
        />
      </Grid>
      <Grid item mr={1}>
        <Item>{props.name}</Item>
      </Grid>
    </Grid>
  );
};

export default ContactItem;