import React, {useState} from 'react';
import {Grid, TextField, Typography} from "@mui/material";
import {LoadingButton} from "@mui/lab";

const AddNewContactForm = ({currentContact, sendBtnLoading, addContact, editContact, contactId}) => {
  const [nameInp, setNameInp] = useState(() => currentContact ? currentContact.name : '');
  const [phoneInp, setPhoneInp] = useState(() => currentContact ? currentContact.phone : '');
  const [emailInp, setEmailInp] = useState(() => currentContact ? currentContact.email : '');
  const [photoInp, setPhotoInp] = useState(() => currentContact ? currentContact.photo : '');

  const onSubmit = (e) => {
    e.preventDefault();
    if (currentContact) {
      editContact({
        name: nameInp,
        phone: phoneInp,
        photo: photoInp,
        email: emailInp,
      }, contactId)
    } else {
      addContact({
        name: nameInp,
        phone: phoneInp,
        photo: photoInp,
        email: emailInp,
      })
    }
    setNameInp('');
    setPhoneInp('');
    setEmailInp('');
    setPhotoInp('');
  };

  return (
    <form onSubmit={onSubmit}>
      <Grid container direction={'column'} spacing={2}>
        <Grid item>
          <TextField
            required
            fullWidth
            label="Name"
            value={nameInp}
            onChange={(e) => setNameInp(e.target.value)}
          />
        </Grid>
        <Grid item>
          <TextField
            required
            fullWidth
            label="Phone"
            value={phoneInp}
            onChange={(e) => setPhoneInp(e.target.value)}
          />
        </Grid>
        <Grid item>
          <TextField
            required
            fullWidth
            label="Email"
            value={emailInp}
            onChange={(e) => setEmailInp(e.target.value)}
          />
        </Grid>
        <Grid item>
          <TextField
            required
            fullWidth
            label="Photo"
            value={photoInp}
            onChange={(e) => setPhotoInp(e.target.value)}
          />
        </Grid>
        <Grid item textAlign={"center"}>
          <Typography variant={"h6"} mb={2}>
            Photo preview:
          </Typography>
          <img
            height="194"
            src={photoInp ? photoInp : 'https://banner2.cleanpng.com/20180630/eae/kisspng-business-lissauer-eriks-dental-group-dentistry-i-5b37c6148893a5.3156102215303818445594.jpg'}
            alt="pic"
          />
        </Grid>
        <Grid item textAlign={"center"}>
          <LoadingButton
            loading={sendBtnLoading}
            type={"submit"}
            variant={"contained"}
            color={"success"}
            size={'large'}
          >
            {currentContact ? 'Edit' : 'Save'}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNewContactForm;