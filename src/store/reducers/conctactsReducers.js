import {
  ADD_NEW_CONTACT_FAILURE,
  ADD_NEW_CONTACT_REQUEST,
  ADD_NEW_CONTACT_SUCCESS, CHANGE_MODAL_OPEN, DELETE_CONTACT_FAILURE, DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS,
  EDIT_CONTACT_FAILURE,
  EDIT_CONTACT_REQUEST,
  EDIT_CONTACT_SUCCESS,
  FETCH_CONTACTS_FAILURE,
  FETCH_CONTACTS_REQUEST,
  FETCH_CONTACTS_SUCCESS, SET_CURRENT_CONTACT, SET_CONTACTS_LOADING
} from "../actions/contactsActions";

const initState = {
  data: null,
  currentContact: null,
  sendBtnLoading: false,
  loading: false,
  error: null,
  contactModalOpen: false,
  deleteLoading: false,
};
export const conctactsReducers = (state = initState, action) => {
  switch (action.type) {
    case CHANGE_MODAL_OPEN:
      return {...state, contactModalOpen: action.payload};
    case SET_CURRENT_CONTACT:
      return {...state, currentContact: action.payload};
    case SET_CONTACTS_LOADING:
      return {...state, loading: action.payload};

    case ADD_NEW_CONTACT_REQUEST:
      return {...state, sendBtnLoading: true, error: null};
    case ADD_NEW_CONTACT_SUCCESS:
      return {...state, sendBtnLoading: false};
    case ADD_NEW_CONTACT_FAILURE:
      return {...state, sendBtnLoading: false, error: action.payload};

    case EDIT_CONTACT_REQUEST:
      return {...state, sendBtnLoading: true, error: null};
    case EDIT_CONTACT_SUCCESS:
      return {...state, sendBtnLoading: false, currentContact: null};
    case EDIT_CONTACT_FAILURE:
      return {...state, sendBtnLoading: false, error: action.payload, currentContact: null};

    case FETCH_CONTACTS_REQUEST:
      return {...state, error: null};
    case FETCH_CONTACTS_SUCCESS:
      return {...state, loading: false, error: null, data: action.payload};
    case FETCH_CONTACTS_FAILURE:
      return {...state, loading: false, error: action.payload};

    case DELETE_CONTACT_REQUEST:
      return {...state, deleteLoading: true, error: null};
    case DELETE_CONTACT_SUCCESS:
      return {...state, deleteLoading: false, currentContact: null, contactModalOpen: false};
    case DELETE_CONTACT_FAILURE:
      return {...state, deleteLoading: false, error: action.payload, currentContact: null, contactModalOpen: false};

    default:
      return state;
  }
}