import {axiosApi} from "../../axiosApi";
import {CONTACTS_URL} from "../../config";

export const ADD_NEW_CONTACT_REQUEST = 'ADD_NEW_CONTACT_REQUEST';
export const ADD_NEW_CONTACT_SUCCESS = 'ADD_NEW_CONTACT_SUCCESS';
export const ADD_NEW_CONTACT_FAILURE = 'ADD_NEW_CONTACT_FAILURE';

export const EDIT_CONTACT_REQUEST = 'EDIT_CONTACT_REQUEST';
export const EDIT_CONTACT_SUCCESS = 'EDIT_CONTACT_SUCCESS';
export const EDIT_CONTACT_FAILURE = 'EDIT_CONTACT_FAILURE';

export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_FAILURE = 'DELETE_CONTACT_FAILURE';

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_FAILURE = 'FETCH_CONTACTS_FAILURE';

export const SET_CONTACTS_LOADING = 'SET_CONTACTS_LOADING';
export const CHANGE_MODAL_OPEN = 'CHANGE_MODAL_OPEN';
export const SET_CURRENT_CONTACT = 'SET_CURRENT_CONTACT';

export const setContactsLoading = (bool) => ({type: SET_CONTACTS_LOADING, payload: bool});
export const changeModalOpen = (bool) => ({type: CHANGE_MODAL_OPEN, payload: bool});
export const setCurrentContact = (data) => ({type: SET_CURRENT_CONTACT, payload: data});

export const addNewContactRequest = () => ({type: ADD_NEW_CONTACT_REQUEST});
export const addNewContactSuccess = () => ({type: ADD_NEW_CONTACT_SUCCESS});
export const addNewContactFailure = (error) => ({type: ADD_NEW_CONTACT_FAILURE, payload: error});

export const addNewContact = (data) => {
  return async (dispatch) => {
    try {
      dispatch(addNewContactRequest());
      await axiosApi.post(CONTACTS_URL + '.json', data);
      dispatch(fetchContacts());
      dispatch(addNewContactSuccess());
    } catch (error) {
      dispatch(addNewContactFailure(error));
    }
  }
};

export const editContactRequest = () => ({type: EDIT_CONTACT_REQUEST});
export const editContactSuccess = () => ({type: EDIT_CONTACT_SUCCESS});
export const editContactFailure = (error) => ({type: EDIT_CONTACT_FAILURE, payload: error});

export const editContact = (data, id) => {
  return async (dispatch) => {
    try {
      dispatch(editContactRequest());
      await axiosApi.put(CONTACTS_URL + `/${id}.json`, data);
      dispatch(fetchContacts());
      dispatch(editContactSuccess());
    } catch (error) {
      dispatch(editContactFailure(error));
    }
  }
};


export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = (data) => ({type: FETCH_CONTACTS_SUCCESS, payload: data});
export const fetchContactsFailure = (error) => ({type: FETCH_CONTACTS_FAILURE, payload: error});

export const fetchContacts = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchContactsRequest());
      const usedState = getState();
      if (!usedState.contacts.data) dispatch(setContactsLoading(true));
      const {data} = await axiosApi.get(CONTACTS_URL + `.json`);
      for (let key in data) {
        data[key].id = key;
      }
      dispatch(fetchContactsSuccess(data));
    } catch (error) {
      dispatch(fetchContactsFailure(error));
    }
  }
}
export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = () => ({type: DELETE_CONTACT_SUCCESS});
export const deleteContactFailure = (error) => ({type: DELETE_CONTACT_FAILURE, payload: error});

export const deleteContact = (key) => {
  return async (dispatch) => {
    try {
      dispatch(deleteContactRequest());
      await axiosApi.delete(CONTACTS_URL + `/${key}.json`);
      dispatch(fetchContacts());
      dispatch(deleteContactSuccess());
    } catch (error) {
      dispatch(deleteContactFailure(error));
    }
  }
}