import Layout from "./componets/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import ChangeContact from "./containers/ChangeContact/ChangeContact";
import Contacts from "./containers/Contacts/Contacts";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts} from "./store/actions/contactsActions";
import BackdropPreloader from "./componets/UI/BackdropPreloader/BackdropPreloader";

const App = () => {
  const loading = useSelector(state => state.contacts.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchContacts());
  }, [dispatch]);
  return (
    <>
      {loading && (
        <BackdropPreloader option={{type: Math.floor(Math.random() * 10)}}/>
      )}
      {!loading && (
        <Layout>
          <Switch>
            <Route path={'/contacts/add'} component={ChangeContact}/>
            <Route path={'/contacts'} component={Contacts}/>
            <Route exact path={'/'} component={Contacts}/>
            <Route component={() => <div>Not Found</div>}/>
          </Switch>
        </Layout>
      )
      }
    </>
  );
};

export default App;
