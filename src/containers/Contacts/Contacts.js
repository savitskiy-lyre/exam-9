import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, Grid, Modal, Typography} from "@mui/material";
import ContactItem from "../../componets/ContactItem/ContactItem";
import {changeModalOpen, deleteContact, setCurrentContact} from "../../store/actions/contactsActions";
import CardMedia from "@mui/material/CardMedia";
import {useHistory} from "react-router-dom";
import {LoadingButton} from "@mui/lab";
import PhoneIphoneIcon from '@mui/icons-material/PhoneIphone';
import AttachEmailIcon from '@mui/icons-material/AttachEmail';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 1,
};

const Contacts = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const contacts = useSelector(state => state.contacts.data);
  const currentContact = useSelector(state => state.contacts.currentContact);
  const contactModalOpen = useSelector(state => state.contacts.contactModalOpen);
  const deleteLoading = useSelector(state => state.contacts.deleteLoading);

  return contacts && (
    <Box maxWidth={'sm'} mx={'auto'}>
      {Object.keys(contacts).map((key) => {
        return (
          <ContactItem
            name={contacts[key]['name']}
            imgUrl={contacts[key]['photo']}
            key={contacts[key].id}
            onClickHandler={() => {
              dispatch(setCurrentContact(contacts[key]))
              dispatch(changeModalOpen(true))
            }}
          />
        );
      })}
      <Modal
        open={contactModalOpen}
        onClose={() => {
          dispatch(setCurrentContact(false))
          dispatch(changeModalOpen(false));
        }}
      >
        <Box sx={style}>
          <Grid container flexWrap={"nowrap"} alignItems={"center"}>
            <Grid item>
              <CardMedia
                component="img"
                height="164"
                image={currentContact?.photo}
                alt={currentContact?.name}
              />
            </Grid>
            <Grid item container direction={'column'} spacing={2} textAlign={"center"}>
              <Grid item>
                <Typography variant={"h3"}>
                  {currentContact?.name}
                </Typography>
              </Grid>
              <Grid item container alignItems={"center"} justifyContent={"center"}>
                <AttachEmailIcon sx={{paddingRight: '10px'}}/>
                <a href={`mailto:${currentContact?.email}`}>{currentContact?.email}</a>
              </Grid>
              <Grid item container alignItems={"center"} justifyContent={"center"}>
                <PhoneIphoneIcon sx={{paddingRight: '10px'}}/>
                <a href={`tel:+${currentContact?.phone}`}>{currentContact?.phone}</a>
              </Grid>
              <Grid item container justifyContent={"space-evenly"} mt={2}>
                <Button
                  onClick={() => {
                    dispatch(changeModalOpen(false));
                    history.push('/contacts/add');
                  }}
                >
                  Edit
                </Button>
                <LoadingButton
                  loading={deleteLoading}
                  onClick={() => dispatch(deleteContact(currentContact.id))}
                >
                  Delete
                </LoadingButton>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </Box>
  );
};

export default Contacts;