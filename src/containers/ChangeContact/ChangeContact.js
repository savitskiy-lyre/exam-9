import React, {useEffect} from 'react';
import AddNewContactForm from "../../componets/AddNewContactForm/AddNewContactForm";
import {useDispatch, useSelector} from "react-redux";
import {Box} from "@mui/material";
import {addNewContact, editContact, setCurrentContact} from "../../store/actions/contactsActions";

const ChangeContact = () => {
  const dispatch = useDispatch();
  const currentContact = useSelector(state => state.contacts.currentContact);
  const sendBtnLoading = useSelector(state => state.contacts.sendBtnLoading);

  useEffect(() => () => dispatch(setCurrentContact(null)));

  return (
    <Box maxWidth={'sm'} mx={'auto'}>
      <AddNewContactForm
        currentContact={currentContact}
        sendBtnLoading={sendBtnLoading}
        addContact={(data) => dispatch(addNewContact(data))}
        editContact={(data, id) => dispatch(editContact(data, id))}
        contactId={currentContact?.id}
      />
    </Box>
  );
};

export default ChangeContact;