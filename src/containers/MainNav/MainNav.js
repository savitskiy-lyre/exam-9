import React from 'react';
import NavTabs from "../../componets/UI/NavTabs/NavTabs";
import {useSelector} from "react-redux";
import {ADD_NEW_CONTACT_ROUTE_LABEL} from "../../constants";

const MainNav = () => {
  const currentContact= useSelector(state => state.contacts.currentContact);

  return (
    <NavTabs
      items={[
        {label: 'Contacts', path: '/contacts'},
        {label: ADD_NEW_CONTACT_ROUTE_LABEL, path: '/contacts/add'},
      ]}
      routes={[
        '/contacts',
        '/contacts/add',
      ]}
      currentContact={currentContact}
    />
  );
};

export default MainNav;